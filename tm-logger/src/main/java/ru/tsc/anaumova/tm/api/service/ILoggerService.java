package ru.tsc.anaumova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.dto.LogDto;

public interface ILoggerService {

    void writeLog(@NotNull LogDto message);

}