package ru.tsc.anaumova.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.anaumova.tm")
public class ApplicationConfiguration {
}