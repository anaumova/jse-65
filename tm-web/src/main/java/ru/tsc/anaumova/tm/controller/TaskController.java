package ru.tsc.anaumova.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.model.Task;
import ru.tsc.anaumova.tm.repository.ProjectRepository;
import ru.tsc.anaumova.tm.repository.TaskRepository;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    private List<Task> getTasks() {
        return taskRepository.findAll();
    }

    @GetMapping("/task/create")
    public String create() {
        taskRepository.add("new task " + System.currentTimeMillis());
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskRepository.removeById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(@ModelAttribute("task") Task task, BindingResult result) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final Task task = taskRepository.findById(id);
        return new ModelAndView("task-edit", "task", task);
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @ModelAttribute("projects")
    public List<Project> getProjects() {
        return projectRepository.findAll();
    }

    @NotNull
    @GetMapping("/tasks")
    public ModelAndView list() {
        return new ModelAndView("task-list", "tasks", getTasks());
    }

}